import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-eliminar-usuario',
  templateUrl: './eliminar-usuario.component.html',
  styleUrls: ['./eliminar-usuario.component.css']
})
export class EliminarUsuarioComponent implements OnInit {

   Correo:any;
  constructor(private service:UsuarioService, private router:Router) { }

  ngOnInit() {
  }

  eliminarUsua(event) {
    event.preventDefault();

    this.service.deletesuario(this.Correo).subscribe((response) => {
      //const res = JSON.parse(response.text());
      console.log(response.text());
      this.Inicio();


    });
  }
  Inicio(){
    return this.router.navigate(["/","inicio"]);
  }

}
