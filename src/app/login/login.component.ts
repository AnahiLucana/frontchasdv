import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  Correo:any;
  Contrasena:any;


  constructor(private service:UsuarioService, private router:Router ) {

  }



  ngOnInit() {
  }

  registrar(event) {
    event.preventDefault();
    var usuario = {
      Correo: this.Correo,
      Contrasena: this.Contrasena

    };
    this.service.postlogUsuario(usuario).subscribe((response) => {
        console.log(response.text());
        //const res = JSON.parse(response.text());


      }
    );


  }
}
