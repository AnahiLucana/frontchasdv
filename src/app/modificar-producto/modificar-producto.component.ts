import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-modificar-producto',
  templateUrl: './modificar-producto.component.html',
  styleUrls: ['./modificar-producto.component.css']
})
export class ModificarProductoComponent implements OnInit {
  Nombre_Pro:any;
  Descripcion:any;
  precio:any;
  constructor(private service: ProductosService, private router: Router) { }

  ngOnInit() {
  }
  modificarProdu(event) {
    event.preventDefault();
    const Producto = {
      Nombre_Pro:this.Nombre_Pro,
    Descripcion:this.Descripcion,
    precio:this.precio

    };
     this.service.putProducto(Producto).subscribe((response) => {
    // const res = JSON.parse(response.text());
       console.log(response.text());


    });
  }

}
