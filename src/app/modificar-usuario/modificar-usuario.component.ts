import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-modificar-usuario',
  templateUrl: './modificar-usuario.component.html',
  styleUrls: ['./modificar-usuario.component.css']
})
export class ModificarUsuarioComponent implements OnInit {
  Nombre:any;
  Contrasena:any;
  factura:any;
  Celular:any;
  Nit:any;
  constructor(private service:UsuarioService, private router:Router) { }

  ngOnInit() {
  }
  modificarUsuario(event) {
    event.preventDefault();
    const Usuario = {
      Nombre:this.Nombre,
      Contrasena:this.Contrasena,
      factura:this.factura,
      Celular:this.Celular,
      Nit:this.Nit


    };
    this.service.putUsuario(Usuario).subscribe((response) => {
      //const res = JSON.parse(response.text());
      console.log(response.text());

    });
  }
}
