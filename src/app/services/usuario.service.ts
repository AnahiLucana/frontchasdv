import { Injectable } from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions} from '@angular/http';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs/internal/Observable";
@Injectable()
export class UsuarioService {
  constructor(
    private http: Http
  ) {
  }

  getUsuario(Usuarios) {
    let url = 'http://localhost:8085/UPB/usuario/get';
    let data = JSON.stringify(Usuarios);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Get,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.get(url, requestOptions);
  }

  postlogUsuario(usuario) {
    let url = 'http://localhost:8085/UPB/usuario/post';
    let data = JSON.stringify(usuario);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
  }
  postUsuario(usuario) {
    let url = 'http://localhost:8085/UPB/usuario/post';
    let data = JSON.stringify(usuario);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
   }

  putUsuario(usuario) {
    let url = 'http://localhost:8085/UPB/usuario/post';
    let data = JSON.stringify(usuario);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Put,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.put(url, data, requestOptions);
  }

  deletesuario(Correo) {
    let url = 'http://localhost:8083/UPB/usuario/delete'+ Correo;
    return this.http.delete(url);
  }

}
