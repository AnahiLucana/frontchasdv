import { Injectable } from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions} from '@angular/http';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs/internal/Observable";
@Injectable()
export class ProductosService {
  constructor(private http: Http) {
  }

  getProducto(Producto) {
    let url = 'http://localhost:8083/UPB/producto/get';
    let data = JSON.stringify(Producto);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Get,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.get(url, requestOptions);
  }

  postProducto(Producto) {
    let url = 'http://localhost:8083/UPB/productos/post';
    let data = JSON.stringify(Producto);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
  }

  putProducto(Producto) {
    let url = 'http://localhost:8083/UPB/producto/put';
    let data = JSON.stringify(Producto);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Put,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.put(url, data, requestOptions);
  }

  deleteProdu(IDProducto) {
    let url = 'http://localhost:8083/UPB/usuario/borrar'+ IDProducto;
    return this.http.delete(url);
  }
}
