import { Component, OnInit } from '@angular/core';
import {Response} from '@angular/http';
import {UsuarioService} from "../services/usuario.service";

import {Router} from "@angular/router";
import {ProductosComponent} from "../productos/productos.component";
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {
  idproductos: any;
  nombre_pro: any;
  descripcion: any;
  precio: any;

  constructor(private service: ProductosService,private router: Router) {
  }

  ngOnInit() {
  }

  crearProdu(event) {
    event.preventDefault();
    const Producto = {
      idproductos: this.idproductos,
      nombre_pro: this.nombre_pro,
      descripcion: this.descripcion,
      precio: this.precio


    };
    this.service.postProducto(Producto).subscribe((response)=> {
      console.log(response);
      // const res = JSON.parse(response.text());


    });

  }
}
